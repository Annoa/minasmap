/* eslint-disable jsx-a11y/anchor-is-valid */

import { useParams } from 'react-router'
import { getAppUrl } from '../../helpers/app'

const ExportButton = (
  { groups, editableLayers, exportFunc, gitFunc }
) => {
  let { id } = useParams()

  const handleExportClick = () => {
    exportFunc(editableLayers.current.toGeoJSON(), groups)
  }
  
  return (
    <div className="top-left-button-container">
      <a className="control-button" onClick={handleExportClick} title="Sauvegarder (envoyer au serveur)" href="#" role="button">
        <i className="fas fa-save fa-lg"></i>
      </a>
      <a className="control-button" onClick={gitFunc} title="Commit et push les modifications sauvegardées" href="#" role="button">
        <i className="fab fa-github fa-lg"></i>
      </a>
      <a className="control-button" title="Télécharger les groupes (depuis le serveur)" 
        href={ getAppUrl() + id +"/groups.json" } target="_blank" rel="noreferrer" >
        <i className="fas fa-download fa-lg"></i><span>G</span>
      </a>
      <a className="control-button" title="Télécharger les données géographiques (depuis le serveur)" 
        href={ getAppUrl() + id +"/map.geojson" } target="_blank" rel="noreferrer" >
        <i className="fas fa-download fa-lg"></i><span>D</span>
      </a>
      <a className="control-button" onClick={() => {}} title="Téléverser (WIP)" href="#" role="button">
        <i className="fas fa-upload fa-lg"></i>
      </a>
    </div>
  )
}

export default ExportButton