import { useState, cloneElement, useEffect } from 'react'
import Modal from 'react-modal'
import axios from 'axios'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'


import CryptoJS from 'crypto-js'
import sha256 from 'crypto-js/sha256'

import { getAppUrl } from './helpers/app'

Modal.setAppElement('#root')

const localFetch = axios.create({ baseURL: getAppUrl() })

const AuthHandler = ({
  children
}) => {
  const [password, setPassword] = useState('')
  const [showPwd, setShowPwd] = useState(false)
  const [modalIsOpen, setModalIsOpen] = useState(false)
  const [isAuth, setIsAuth] = useState(false)
  const [error, setError] = useState(false)

  useEffect(() => {
    const config = {headers: { Authorization: `Bearer ${window.localStorage.getItem('minas_token')}` }}
    localFetch.get('tokenIsValid', config).then(res => {
      console.log('tokenIsValid', res)
      setIsAuth(true)
    }).catch(err => {
      setIsAuth(false)
    })
  }, [])

  const authenticate = (password) => {
    const cryptedPwd = sha256(password).toString(CryptoJS.enc.Base64)
    localFetch.post('login', {password:cryptedPwd}).then(res => {
      window.localStorage.setItem('minas_token', res.data)
      setIsAuth(true)
      setPassword('')
      closeModal()
    }).catch(err => {
      setIsAuth(false)
      console.error(err)
      setError(`Echec de l'authentification: ${err?.message}`)
    })
  }

  const signout = (cb) => {
    setIsAuth(false)
    window.localStorage.removeItem('minas_token')
  }

  const openModal = () => {
    setShowPwd(false)
    setError(false)
    setPassword('')
    setModalIsOpen(true)
  }
  const closeModal = () => setModalIsOpen(false)
  const handleChange = (e) => setPassword(e.target.value)
  const toggleShowPwd = () => setShowPwd(!showPwd)

  const handleSubmit = (e) => {
    authenticate(password)
    e.preventDefault()
  }

  const AuthButton = <div className="nav-button nav-button-login" onClick={isAuth?signout:openModal}>
    <FontAwesomeIcon icon={["fas", isAuth?'user-alt-slash':'user-alt']} />
    {!isAuth &&<span className="resp-display-lg">Login</span>}
  </div>

  return <>
    { cloneElement(children, { AuthButton, isAuth }) }
    <Modal
      isOpen={modalIsOpen}
      onRequestClose={closeModal}
      contentLabel="Login"
      className="modal"
      overlayClassName="overlay"
    >
      <div className="modal-header">
        <div>Login</div>
        <div className="cursor-pointer" onClick={closeModal}>
          <i className="fas fa-window-close"></i>
        </div>
      </div>
      <div className="modal-content">
        <form className="login-form" onSubmit={handleSubmit}>
          <input value={password} type={showPwd?'text':'password'} onChange={handleChange}/>
          <span onClick={toggleShowPwd}><FontAwesomeIcon icon={["fas", showPwd?'eye-slash':'eye']} /></span>
          <span onClick={handleSubmit}><FontAwesomeIcon icon={["fas", "check-circle"]} /></span>
        </form> 
        { error && <div>{error}</div>}
      </div>
    </Modal>
  </>
}

export default AuthHandler