import { useEffect, useState } from "react"
import { DEFAULT_COLOR } from '../../constants'
import {ReactComponent as Trash } from '../../assets/icons/trash-alt-regular.svg'
import {ReactComponent as Add } from '../../assets/icons/plus-solid.svg'
import {ReactComponent as Undo } from '../../assets/icons/undo-solid.svg'
import {ReactComponent as Save } from '../../assets/icons/save-solid.svg'
import SvgButton from "./SvgButton"

const EditGroupsControl = ({ 
  groups,
  saveGroups 
}) => {
  const [groups_, setGroups_] = useState([])

  useEffect(() => {
    setGroups_(groups)
  }, [groups])
  
  const updateGroupName = (name, id) => {
    const newGroup = groups_.map(g => ({...g, name: g.id === id? name:g.name}))
    setGroups_(newGroup)
  }

  const updateGroupColor = (color, id) => {
    const newGroup = groups_.map(g => ({...g, color: g.id === id? color:g.color}))
    setGroups_(newGroup)
  }
  
  const addGroup = () => {
    const id = groups_.reduce((acc,item)=> Math.max(acc, item.id), 0) + 1
    setGroups_([...groups_, {
      id,
      name: 'Nom',
      color: DEFAULT_COLOR
    }])
  }

  const deleteGroup = (id) => {
    setGroups_(groups_.filter(g => g.id !== id))
  }

  useEffect(() => {
    const handlerSave = (e) => {
      if (e.code === 'Enter') {
        saveGroups(groups_)
      }
      else if (e.code === 'Escape') {
        saveGroups(groups)
      }
    }
    window.addEventListener('keydown', handlerSave)
    return () => {
      window.removeEventListener("keydown", handlerSave)
    }
  }, [saveGroups, groups_, groups])

  return (
    <div className="control groups-control">
      <div className="groups-control-header">
        <div className="group-container">
          <input type="color" /> 
          <label>Groupes</label>
        </div>
      </div>

      {groups_.map(g => 
        <div className="group-container" key={'group'+g.id}>
            <input type="color" value={g.color} onChange={e => updateGroupColor(e.target.value, g.id)}/>
            <input type="text" value={g.name} onChange={e => updateGroupName(e.target.value, g.id)}/>
            <SvgButton onClick={() => deleteGroup(g.id)}><Trash/></SvgButton>
        </div>
      )}
      
      <div className="edition-buttons-container">
        <SvgButton onClick={addGroup}><Add/></SvgButton>
        <SvgButton onClick={() => saveGroups(groups_)}><Save/></SvgButton>
        <SvgButton onClick={() => saveGroups(groups)}><Undo/></SvgButton>
      </div>
      <div className="text-align-center">
        <i>Press Enter to save, Escape to cancel</i>
      </div>
    </div>
  )
}

export default EditGroupsControl