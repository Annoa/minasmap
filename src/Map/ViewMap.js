import { useEffect, useRef, useState } from 'react'
import L from 'leaflet'
import mapsBg from '../assets/bg'
import { MarkerIcon, PingIcon } from './Icon'
import PropsControl from './Controls/PropsControl'
import GroupsControl from './Controls/GroupsControl'
import { isPointInsidePolygon, isPointInsidePolyline, setGroupStyle } from '../helpers/map'
import { MAP_BOUNDS } from '../constants'
import socketIOClient from 'socket.io-client'
import { getAppUrl } from '../helpers/app'


const pingMap = ({latlng, map}) => {
  const ping = L.marker(latlng).setIcon(PingIcon()).addTo(map)
  setTimeout(() => {
    ping.removeFrom(map)
  }, 5000)
}

const ViewMap = ({ geojson, groups, id }) => {
  const map = useRef(null)
  const mapLayers = useRef(null)
  const socket = useRef(null)

  const [visibleGroups, setVisibleGroups] = useState({})
  const [mouseOverLayers, setMouseOverLayers] = useState({})

  const onChangeVisibility = (groupId, visible) => {
    if (groupId === 'ALL' && visible) {
      setVisibleGroups({...Object.fromEntries(groups.map(g => [g.id, visible]))})
    }
    else if (groupId === 'ALL' && !visible) {
      setVisibleGroups({})
    }
    else {
      setVisibleGroups({...visibleGroups, [groupId]: visible})
    }
    mapLayers.current.eachLayer(layer => {
      setGroupStyle(layer, groups, visibleGroups)
    })
  }

  useEffect(() => {
    const onLayerAdd = (e) => {
      var layer = e.layer
      if (layer instanceof L.Marker) {
        layer.bindTooltip(layer.feature.properties.name, {direction:'top'})
      }
    }
    mapLayers.current = new L.geoJSON(null, {
      pointToLayer: function (feature, latlng) {
        return L.marker(latlng, {icon: MarkerIcon(feature.properties?.icon)})
      }
    })
    mapLayers.current.on('layeradd', onLayerAdd)
    mapLayers.current.clearLayers()
    mapLayers.current.addData(geojson)
    setMouseOverLayers([])
  }, [geojson])

  useEffect(() => {
    mapLayers.current.eachLayer(layer => {
      setGroupStyle(layer, groups, visibleGroups)
    })
  }, [visibleGroups, groups])

  useEffect(() => {
    if (socket.current) return
    socket.current = socketIOClient(getAppUrl())
    socket.current.on("ping", data => {
      pingMap({map:map.current, latlng:data.latlng})
    })
  }, [])

  useEffect(() => {
    if (map.current) return
    map.current = L.map('map', {
      crs: L.CRS.Simple,
      minZoom: -5
    })

    map.current.addLayer(mapLayers.current)
    
    L.imageOverlay(mapsBg[id], MAP_BOUNDS).addTo(map.current)
    map.current.setView( [500, 500], 0)

    map.current.on('click', (e) => {
      pingMap({map:map.current, latlng:e.latlng})
      socket.current.emit('ping', {latlng:e.latlng})
    })

    map.current.on('mousemove', e => {
      const layers = []
      // Todo: ignore hidden Layer with a display none class ?
      mapLayers.current.eachLayer(l => {
        if (l instanceof L.Polygon) {
          if (isPointInsidePolygon(e.latlng, l)) 
            layers.push(l)
        }
        else if (l instanceof L.Polyline) {
          if (isPointInsidePolyline(map.current, e.latlng, l)) {
            layers.push(l)
          }
        }
      })
      setMouseOverLayers([...layers])
    })
  }, [id])
  
  return (
    <>
      <style type="text/css" scoped>
        {".leaflet-interactive { cursor: default; } "}
      </style>
      <div id="map"></div>
      <div className="top-right-control-container">
        <GroupsControl
          groups={groups}
          visibleGroups={visibleGroups}
          onChangeVisibility={onChangeVisibility}
          readOnly/>
        <PropsControl 
          isMarker
          polyLayers={mouseOverLayers} 
          groups={groups}
          readOnly/>
      </div>
    </>
  )
}

export default ViewMap