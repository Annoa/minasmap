import L from 'leaflet'

import * as Markers from '../assets/markers'
import ReactDOMServer from 'react-dom/server'

const getIconAnchor = (name) => {
  const bottomAnchor = ['MapMarkerAltSolid', 'MapMarkerSolid', 'MapPinSolid']
  return bottomAnchor.includes(name)? [9, 22] : [9, 11]
}

export const MarkerIcon = (name) => {
  let name_ = Object.keys(Markers).includes(name) ? name : 'MapMarkerAltSolid'
  const CompName = Markers[name_]
  return L.divIcon({ 
    html: ReactDOMServer.renderToStaticMarkup(<CompName/>), 
    iconSize: [18, 22], 
    iconAnchor: getIconAnchor(name_), 
    tooltipAnchor: [0, -25],
    className: 'my-div-icon' 
  })
}

export const InlineMarker = ({name}) => {
  let name_ = Object.keys(Markers).includes(name) ? name : 'MapMarkerAltSolid'
  const CompName = Markers[name_]
  return <div className="marker-svg"><CompName/></div>
}

export const PingIcon = () => {
  return L.divIcon({ 
    html: ReactDOMServer.renderToStaticMarkup(<div className="ping-container">
      <div className="ping"></div>
    </div>),
    className: 'dummy'
  })
}