import { useState } from 'react'
import EditGroupsControl from './EditGroupsControl'
import { isDarkColor } from '../../helpers/controls'
import SvgButton from './SvgButton'
import {ReactComponent as Edit } from '../../assets/icons/edit-solid.svg'
import {ReactComponent as Minimize } from '../../assets/icons/window-minimize-solid.svg'
import {ReactComponent as Maximize } from '../../assets/icons/window-maximize-regular.svg'

const GroupsControl = ({ 
  groups, 
  onChangeVisibility, 
  visibleGroups, 
  saveGroups,
  readOnly
}) => {
  const [editing, setEditing] = useState(false)
  const [isMinimized, setIsMinimized] = useState(false)

  const handleVisibilityChange = (e) => {
    const id = e.target.name
    const visible = e.target.checked
    onChangeVisibility(id, visible)
  }

  const handleSaveGroup = (params) => {
    setEditing(false)
    saveGroups(params)
  }

  const toggleMinimize = () => {
    setIsMinimized(!isMinimized)
  }

  const startEditing = () => {
    if (!readOnly) setEditing(true)
  }

  if (editing) 
    return <EditGroupsControl groups={groups} saveGroups={handleSaveGroup}/>

  return (
    <div className={`control groups-control ${isMinimized? 'minimized':''}`}>
      <div className="groups-control-header">
        <div className="group-container">
          <input type="checkbox" name="ALL" checked={groups.every(g => visibleGroups[g.id])} onChange={handleVisibilityChange}/> 
          <label>Groupes</label>
          {!readOnly && <SvgButton onClick={startEditing}><Edit/></SvgButton>}
          {readOnly && <SvgButton onClick={toggleMinimize}>{isMinimized?<Maximize/>:<Minimize/>}</SvgButton>}
        </div>
      </div>

      <div className="groups-control-body">
        {groups.map(g => 
          <div className="group-container" key={'group'+g.id}>
              <input type="checkbox" name={g.id} checked={visibleGroups[g.id] || false} onChange={handleVisibilityChange} />
              <label className={`${isDarkColor(g.color)? 'group-label-dark':'group-label-light'}`} style={{color: g.color}}>
                {g.name}
              </label>
          </div>
        )}
      </div>
    </div>
  )
}

export default GroupsControl