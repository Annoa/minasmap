import React from 'react'
import ReactDOM from 'react-dom'
import App from './App'
import 'leaflet-draw'
import 'leaflet-draw/dist/leaflet.draw.css'
import 'leaflet/dist/leaflet.css'
import './styles/variables.css'
import './assets/fonts/fonts.css'
import './styles/index.css'
import './styles/app.css'
import './styles/map.css'
import './styles/controls.css'
import './styles/snackbar.css'
import '@fortawesome/fontawesome-free/js/fontawesome'
import '@fortawesome/fontawesome-free/js/solid'
import '@fortawesome/fontawesome-free/js/regular'
import '@fortawesome/fontawesome-free/js/brands'
import AuthHandler from './AuthHandler'

ReactDOM.render(
  <React.StrictMode>
    <AuthHandler>
      <App />
    </AuthHandler>
  </React.StrictMode>,
  document.getElementById('root')
)