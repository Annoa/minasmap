import { useEffect, useRef, useState } from 'react'
import L from 'leaflet'
import mapsBg from '../assets/bg'
import { MarkerIcon } from './Icon'
import PropsControl from './Controls/PropsControl'
import GroupsControl from './Controls/GroupsControl'
import ExportButton from './Controls/ExportButton'
import { drawControlsOptions } from '../helpers/controls'
import { setGroupStyle, isPointInsidePolygon, isPointInsidePolyline, addEditingEvent } from '../helpers/map'
import { MAP_BOUNDS } from '../constants'


const EditableMap = ({ id, geojson, groups, saveGroups, exportFunc, gitFunc }) => {
  const map = useRef(null)
  const editableLayers = useRef(null)
  const lastEditingState = useRef(false)
  const [visibleGroups, setVisibleGroups] = useState({})

  // mouse over handler
  const [overedMarker, setOveredMarker] = useState(null)
  const [mouseOverLayers, setMouseOverLayers] = useState({})
  const [editing, setEditing] = useState(null)

  const nbLayers = editableLayers.current?.getLayers()?.length

  const handleClick = (e) => {
    lastEditingState.current = !lastEditingState.current
    setEditing(lastEditingState.current)
    L.DomEvent.stopPropagation(e)
  }

  const handleMouseOver = (e) => {
    setOveredMarker(e.target)
  }

  const handleMouseOut = (e) => {
    setOveredMarker(null)
  }

  const saveGroupCascade = (newGroups) => {
    groups.forEach(g => {
      if (!newGroups.find(gg => gg.id === g.id)) {
        editableLayers.current.eachLayer(layer => {
          if (layer.feature.properties?.groupId === g.id) {
            delete layer.feature.properties.groupId
            setGroupStyle(layer, newGroups, visibleGroups)
          }
        })
        if (visibleGroups[g.id]) {
          const {[g.id]:tmp, ...visibleG} = visibleGroups
          setVisibleGroups(visibleG)
        }
      }
    })
    saveGroups(newGroups)
  }

  const saveNewProps = newProps => {
    editableLayers.current.eachLayer(layer => {
      if (newProps.id === layer.feature.properties.id) {
        layer.feature.properties = newProps
        //setMouseOverLayers([...mouseOverLayers])
        setGroupStyle(layer, groups, visibleGroups)
      }
    })
    setOveredMarker({...overedMarker})
    setMouseOverLayers({...mouseOverLayers})
  }

  const onChangeVisibility = (groupId, visible) => {
    if (groupId === 'ALL' && visible) {
      setVisibleGroups({...Object.fromEntries(groups.map(g => [g.id, visible]))})
    }
    else if (groupId === 'ALL' && !visible) {
      setVisibleGroups({})
    }
    else {
      setVisibleGroups({...visibleGroups, [groupId]: visible})
    }
    editableLayers.current.eachLayer(layer => {
      setGroupStyle(layer, groups, visibleGroups)
    })
  }

  useEffect(() => {
    const onLayerAdd = (e) => {
      console.log('ADD LAYER')
      var layer = e.layer
      layer.feature = layer.feature || {} 
      layer.feature.type = layer.feature.type || "Feature"
      layer.feature.properties = layer.feature.properties || {}
      layer.feature.properties.name = layer.feature.properties.name || 'My name'
      layer.feature.properties.id = layer._leaflet_id
      layer.on('click', handleClick)
      if (layer instanceof L.Marker) {
        layer.on('mouseover', handleMouseOver)
        layer.on('mouseout', handleMouseOut)
        layer.bindTooltip(layer.feature.properties.name, {direction:'top'})
      }
    }
    editableLayers.current = new L.geoJSON(null, {
      //bubblingMouseEvents: false,
      pointToLayer: function (feature, latlng) {
        return L.marker(latlng, {icon: MarkerIcon(feature.properties?.icon)})
      }
    })
    editableLayers.current.on('layeradd', onLayerAdd)
  }, [])

  useEffect(() => {
    editableLayers.current.clearLayers()
    editableLayers.current.addData(geojson)
    setMouseOverLayers([])
  }, [geojson])

  useEffect(() => {
    editableLayers.current.eachLayer(layer => {
      layer.feature = layer.feature || {} 
      layer.feature.type = layer.feature.type || "Feature"
      layer.feature.properties = layer.feature.properties || {}
      layer.feature.properties.name = layer.feature.properties.name || 'My name'
      layer.feature.properties.id = layer._leaflet_id
      setGroupStyle(layer, groups, visibleGroups)
    })
  }, [geojson, nbLayers, visibleGroups, groups])

  useEffect(() => {
    if (map.current) return
    map.current = L.map('map', {
      crs: L.CRS.Simple,
      minZoom: -5
    })
    
    L.Marker.prototype.options.icon = MarkerIcon() // DELETE ME ?
    
    map.current.addLayer(editableLayers.current)

    var drawControl = new L.Control.Draw(drawControlsOptions(editableLayers.current))
    map.current.addControl(drawControl)
    addEditingEvent({map, editableLayers, setOveredMarker})

    L.imageOverlay(mapsBg[id], MAP_BOUNDS).addTo(map.current)
    map.current.setView( [500, 500], 0)

    map.current.on('click', e => {
      setEditing(false)
    })

    map.current.on('mousemove', e => {
      const layers = []
      // Todo: ignore hidden Layer with a display none class ?
      editableLayers.current.eachLayer(l => {
        if (l instanceof L.Polygon) {
          if (isPointInsidePolygon(e.latlng, l)) 
            layers.push(l)
        }
        else if (l instanceof L.Polyline) {
          if (isPointInsidePolyline(map.current, e.latlng, l)) {
            layers.push(l)
          }
        }
      })
      setMouseOverLayers([...layers])
    })
  }, [id])

  return (
    <>
      <div id="map"></div>
      <ExportButton
        groups={groups}
        editableLayers={editableLayers}
        exportFunc={exportFunc}
        gitFunc={gitFunc}
        />
      <div className="top-right-control-container">
        <GroupsControl
          groups={groups}
          visibleGroups={visibleGroups}
          onChangeVisibility={onChangeVisibility}
          saveGroups={saveGroupCascade} />
        <PropsControl 
          isMarker
          markerLayer={overedMarker}
          polyLayers={mouseOverLayers} 
          editing={editing}
          groups={groups}
          onSaving={saveNewProps} />
      </div>
    </>
  )
}

export default EditableMap