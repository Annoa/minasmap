import L from 'leaflet'
import { MarkerIcon } from '../Map/Icon'
import tinycolor from 'tinycolor2'

const addControl = (map, ref, options) => {
  const control = L.control(options)
  control.onAdd = () => ref
  control.addTo(map.current)
  L.DomEvent.disableClickPropagation(ref)
}

const drawControlsOptions = (editableLayers) => ({
  position: 'topleft',
  draw: {
    //polyline: {shapeOptions: {color: '#f357a1', weight: 10}},
    polygon: {
      allowIntersection: false, // Restricts shapes to simple polygons
      drawError: {
        color: '#e1e100', // Color the shape will turn when intersects
        message: '<strong>Oh snap!<strong> you can\'t draw that!' // Message that will show when intersect
      },
      shapeOptions: {},
    },
    marker: {icon: MarkerIcon() },
    circle: false,
    rectangle: false,
    circlemarker: false
  },
  edit: {featureGroup: editableLayers}
})

const isDarkColor = (colorString) => {
  var color = tinycolor(colorString)
  return (color.getBrightness() < 150)
}

export { addControl, drawControlsOptions, isDarkColor }