import * as Markers from '../../../assets/markers'

const IconInput = ({defaultValue, handleIconChange}) => {
  const markersArray = []
  for (const [key, Value] of Object.entries(Markers)) {
    markersArray.push(<label key={key}>
        <input type="radio" 
          name="marker" 
          value={key} 
          checked={key === defaultValue}
          onChange={() => handleIconChange(key)}
          />
        <div className="marker-svg"><Value/></div>
      </label>
    )
  }
  return <div className="svg-grid">
    { markersArray }
  </div>
}

export default IconInput