import L from 'leaflet'
import { DEFAULT_COLOR } from '../constants'
import { MarkerIcon } from '../Map/Icon'

export const setGroupStyle = (layer, groups, visibleGroups) => {
  const groupId = layer.feature.properties?.groupId
  let visible = true, color = (layer instanceof L.Marker) ? '#000':DEFAULT_COLOR
  if (groupId || groupId === 0) {
    visible = visibleGroups[groupId] ?? false
    color = groups.find(g => g.id === groupId)?.color ?? DEFAULT_COLOR
  }
  if (layer instanceof L.Marker) {
    const { icon, name } = layer.feature.properties
    layer.getElement() && (layer.getElement().style.color = color)
    layer.setOpacity(visible? 1 : 0)
    icon && layer.setIcon(MarkerIcon(layer.feature.properties?.icon))
    layer.setTooltipContent(name || '')
    return
  }
  if (visible) {
    layer.setStyle({color, fillColor:color, opacity: 0.8, fillOpacity: 0.3, weight:1})
  } else {
    layer.setStyle({opacity:0, fillOpacity:0})
  }
}


// https://stackoverflow.com/questions/29736345/adding-properties-to-a-leaflet-layer-that-will-become-geojson-options
export const addEditingEvent = ({map, editableLayers, setOveredMarker}) => {
  map.current.on(L.Draw.Event.CREATED, function (e) {
    //var type = e.layerType,
    var layer = e.layer
    editableLayers.current.addLayer(layer)
  })
  map.current.on(L.Draw.Event.DELETED, function (e) {   
    e.layers.eachLayer(layer => {
      setOveredMarker(null)
    }) 
  })
  map.current.on(L.Draw.Event.EDITED, function (e) { 
    // e.layers.eachLayer(layer => {
    //   // editableLayers.current.removeLayer(layer)
    //   // editableLayers.current.addLayer(layer)
    // })   
  })
}

// Would benefit from https://github.com/Leaflet/Leaflet/issues/4461
export function addGeojsonToEditableLayers(sourceLayer, targetGroup) {
  if (sourceLayer instanceof L.LayerGroup) {
    sourceLayer.eachLayer(function(layer) {
      addGeojsonToEditableLayers(layer, targetGroup);
    })
  } else {
    targetGroup.addLayer(sourceLayer);
  }
}

// only work for contiguous polygons
// https://stackoverflow.com/questions/31790344/determine-if-a-point-reside-inside-a-leaflet-polygon
export function isPointInsidePolygon(latlng, poly) {
  var inside = false
  var x = latlng.lat, y = latlng.lng
  for (var ii=0;ii<poly.getLatLngs().length;ii++){
    var polyPoints = poly.getLatLngs()[ii]
    for (var i = 0, j = polyPoints.length - 1; i < polyPoints.length; j = i++) {
      var xi = polyPoints[i].lat, yi = polyPoints[i].lng
      var xj = polyPoints[j].lat, yj = polyPoints[j].lng

      var intersect = ((yi > y) !== (yj > y))
          && (x < (xj - xi) * (y - yi) / (yj - yi) + xi)
      if (intersect) inside = !inside
    }
  }
  return inside
}

export function isPointInsidePolyline(map, latlng, poly) {
  // for (var i=0;i<poly.getLatLngs().length - 1;i++) {
  //   var p1 = poly.getLatLngs()[i], p2 = poly.getLatLngs()[i+1]
  //   let bounds = L.latLngBounds(p1, p2)
  //   if (bounds.contains(latlng)) 
  //     return true
  // }
  var p = map.latLngToLayerPoint(latlng)
  for (var i=0;i<poly.getLatLngs().length - 1;i++) {
    var p1 = map.latLngToLayerPoint(poly.getLatLngs()[i]), 
      p2 = map.latLngToLayerPoint(poly.getLatLngs()[i+1])
    if (L.LineUtil.pointToSegmentDistance(p, p1, p2) < 10) 
      return true
  }
  return false;
}