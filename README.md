
# Minas-Yassë map

## dev mode

```
npm install
npm run dev-server
npm run start
```

* server on http://localhost:9000 (nodemon)
* client on http://localhost:3000

## prod mode & local map edition

```
npm install
npm run build
node server.js || npm run server-dev
```

* http://localhost:3000

## prod with docker

```
docker build -t concept:1.0 .
docker run --name concept-web -p 8080:3000 concept:1.0
```

## prod with heroku

To create
```
heroku login
heroku create minas-map
heroku config:set TOKEN_SECRET=...
heroku config:set ADMIN_PASSWORD=...
```

To deploy
```
heroku login
git push heroku master
heroku ps:scale minas-map=1 #1 = nb dynamo
heroku open
```

View logs
```
heroku logs --tail
```

Run locally
```
heroku local web
```

Start a console inside dynamo
```
heroku run bash
```

* /!\ Horoku use env variable $PORT
* Docs : [https://devcenter.heroku.com/articles/getting-started-with-nodejs?singlepage=true#view-logs]


## SVG

https://fontawesome.com