#!/bin/bash

git init
mkdir ~/.ssh 
touch ~/.ssh/id_dsa
echo "Host gitlab.com
  StrictHostKeyChecking no" > ~/.ssh/config
echo "$GIT_PRIVATE_KEY" > ~/.ssh/id_dsa
git remote add origin git@gitlab.com:Annoa/minasmap.git
git fetch
git reset origin/master

MAP_ID=$1
git config --local user.email "aramile@hotmail.fr"
git config --local user.name "MinasBot"
git add ./server/data/$1/*
git commit -m "New data on $1"
git push origin master