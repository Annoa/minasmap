import { useCallback, useEffect, useState } from "react"

const isString = (s) => typeof s === 'string'

const Snackbar = ({
  content,
  isError
}) => {
  const [animClass, setAnimClass] = useState(false)

  useEffect(()=> {
    // to restart the css animation
    setAnimClass(false)
    content && setTimeout(() => setAnimClass(true), 0)
  }, [content, isError])

  useEffect(() => {
    if (animClass) {
      setTimeout(() => setAnimClass(false), 5000)
    }
  }, [animClass])

  return <div id="snackbar" className={`${animClass?'show':''} ${isError?'error':''} `}>
    { Array.isArray(content) &&
      content.filter(isString).map((l,i) => <div key={i}>{l}</div>)
    }
    { isString(content) &&
      content
    }
  </div>
}

// todo: est-ce qu'un hook peut retourner un component avec ses props ?

const useSnackbar = () => {
  const [snackbarContent, setSnackbarContent] = useState('')
  const [snackbarError, setSnackbarError] = useState(false)

  const displaySnackbar = useCallback((content, isError = false) => {
    setSnackbarContent(content)
    setSnackbarError(isError)
  }, [setSnackbarContent, setSnackbarError])

  return {
    displaySnackbar,
    snackbarContent,
    snackbarError
  }
}

export default Snackbar
export { useSnackbar }