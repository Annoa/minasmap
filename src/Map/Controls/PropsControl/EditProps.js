import React, { useCallback, useEffect, useState } from "react"
import IconInput from './IconInput'
import { InlineMarker } from '../../Icon'
import SvgButton from '../SvgButton'
import { ReactComponent as Edit } from '../../../assets/icons/edit-solid.svg'
import { ReactComponent as Save } from '../../../assets/icons/save-solid.svg'
import {ReactComponent as Undo } from '../../../assets/icons/undo-solid.svg'
import { isDarkColor } from "../../../helpers/controls"

const EditPropsForm = (
  { properties, onSaving, groups, isMarker, readOnly }, 
) => {
  const [editing, setEditing] = useState(false)
  const [props, setProps] = useState({})

  useEffect(() => {
    setProps({...properties})
  }, [properties])

  const handleSave = useCallback(() => {
    setEditing(false)
    onSaving(props)
  }, [props, onSaving])

  const handleUndo = useCallback(() => {
    setEditing(false)
    setProps({...properties})
  }, [properties])

  const startEditing = () => {
    setEditing(true)
  }

  const handleNameChange = e => {
    setProps({...props, name:e.target.value})
  }
  const handleGroupChange = e => {
    setProps({...props, groupId:parseInt(e.target.value)})
  }
  const handleIconChange = value => {
    setProps({...props, icon:value})
  }

  useEffect(() => {
    const keyDownHandler = (e) => {
      if (e.code === 'Enter') 
        handleSave()
      else if (e.code === 'Escape') 
        handleUndo()
    }
    window.addEventListener('keydown', keyDownHandler)
    return () => {
      window.removeEventListener("keydown", keyDownHandler)
    }
  }, [handleSave, handleUndo])

  const groupId = (props.groupId || props.groupId===0) ? ''+props.groupId:''

  if (editing) {
    return (
      <div className="layer-info edit-props">

        <label className="text-label">Name</label>
        <input type="text" value={props.name||''} onChange={handleNameChange}/>

        <br/><label className="text-label">Groupe</label>
        <select value={''+groupId} onChange={handleGroupChange}>
          <option value="">Aucun groupe</option>
          {groups.map(g => <option 
            key={g.name} value={''+g.id}>
              {g.name}
            </option>)}
        </select> 

        { isMarker &&
          <IconInput defaultValue={props.icon || ''} handleIconChange={handleIconChange}/>
        }
        <div className="edition-buttons-container">
          <SvgButton onClick={handleSave}><Save/></SvgButton>
          <SvgButton onClick={handleUndo}><Undo/></SvgButton>
        </div>
        <div className="text-align-center">
          <i>Press Enter to save, Escape to cancel</i>
        </div>
      </div>
    )
  }

  const group = groups.find(g => g.id === properties.groupId)

  return (
    <div className="layer-info">
      { readOnly ?
        <div className="text-align-center">
          <label className={`${isDarkColor(group?.color)? 'group-label-dark':'group-label-light'}`} style={{color: group?.color}}>
            {group?.name}
          </label>
          <br/>
          {properties.name}
        </div>
      :
        <>
          <label className="text-label">Name</label> {properties.name} <br/>
          <label className="text-label">Groupe</label> {group?.name }<br/>
          {(isMarker) && <><label className="text-label">Icon</label> <InlineMarker name={properties?.icon}/></>}
          <div className="edition-buttons-container">
            <SvgButton onClick={startEditing}><Edit/></SvgButton>
          </div>
        </>
      }
    </div>
  )
}

export default EditPropsForm