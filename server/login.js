const jwt = require('jsonwebtoken')

function generateAccessToken(username) {
  return jwt.sign(username, process.env.TOKEN_SECRET, { expiresIn: '60d' });
}

function authenticateToken(req, res, next) {
  const authHeader = req.headers['authorization']
  const token = authHeader && authHeader.split(' ')[1]

  if (token == null) return res.sendStatus(401)
  
  jwt.verify(token, process.env.TOKEN_SECRET, (err, user) => {
    
    if (err) { 
      console.log(err?.name, err?.message)
      return res.sendStatus(403)
    }

    req.user = user

    next()
  })
}
module.exports = { generateAccessToken, authenticateToken }