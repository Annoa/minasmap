import { useEffect, useState } from "react"
import EditProps from "./EditProps"
import { ReactComponent as Lock } from '../../../assets/icons/lock-solid.svg'
import { ReactComponent as LockOpen } from '../../../assets/icons/lock-open-solid.svg'


const PropsControl = (
  { polyLayers, markerLayer, onSaving, groups, editing, readOnly }
) => {
  const [layers, setLayers] = useState([])
  const [marker, setMarker] = useState(false)
  
  useEffect(() => {
    if (!editing) {
      polyLayers && setLayers(Object.values(polyLayers).filter(l => l?.feature?.properties))
      setMarker(markerLayer)
    }
  }, [polyLayers, markerLayer, editing])
  
  const visible = marker || layers.length

  return (
    <div className="control" style={{opacity: visible? 1:0}}>
      {!readOnly && 
        <h4>Properties 
        <div className="info-icon"><div className="svg-icon-container">{editing? <Lock/>:<LockOpen/>} </div></div>
        </h4>
      }
      { marker?.feature?.properties && <EditProps 
          properties={marker.feature.properties}
          onSaving={onSaving}
          groups={groups}
          readOnly={readOnly}
          isMarker />
      }
      {
        layers.map(l => <EditProps key={l.feature.properties.id} 
          properties={l.feature.properties}
          onSaving={onSaving}
          groups={groups}
          readOnly={readOnly} />
        )
      }
    </div>
  )
}

export default PropsControl