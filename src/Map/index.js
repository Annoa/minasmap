import { useState, useEffect, useCallback } from "react"
import EditableMap from './EditableMap'
import ViewMap from './ViewMap'
import axios from 'axios'
import { Redirect, Route, Switch, useParams, useRouteMatch } from "react-router"
import { getAppUrl } from '../helpers/app'

const localFetch = axios.create({baseURL: getAppUrl() })

const MapContainer = ({isAuth}) => {
  let match = useRouteMatch()
  let { id } = useParams()

  const [geojson, setGeojson] = useState(null)
  const [groups, setGroups] = useState(null)
  const [snackbarContent, setSnackbarContent] = useState('')
  const [snackbarError, setSnackbarError] = useState(false)

  const displaySnackBar = useCallback((content, isError=false) => {
    if (isError) {
      console.error(content)
      setSnackbarContent(content.toString())
      setSnackbarError(true)
    }
    else {
      if (content instanceof Array) {
        setSnackbarContent(content.map((l,i) => <p key={i}>{l}</p>))
      }
      else {
        setSnackbarContent(content)
      }
    }
    setTimeout(() => { setSnackbarContent(''); setSnackbarError(false) }, 5000)
  }, [])

  useEffect(() => {
    if (!id)
      return
    setGeojson(null)
    setGroups(null)
    localFetch.get(id+'/geojson').then(res => {
      console.log(res.data)
      setGeojson(res.data)
    }).catch(err => {
      displaySnackBar(err, true)
    })
    localFetch.get(id+'/groups').then(res => {
      console.log(res.data)
      setGroups(res.data)
    }).catch(err => {
      displaySnackBar(err, true)
    })
  }, [id, displaySnackBar])

  const exportFunc = (currGeojson, currGroups) => {
    const config = {headers: { Authorization: `Bearer ${window.localStorage.getItem('minas_token')}` }}
    Promise.all([
      localFetch.post(id+'/geojson', currGeojson, config),
      localFetch.post(id+'/groups', currGroups, config),
    ]).catch(err => {
      displaySnackBar(err, true)
    }).then(() => {
      setGeojson(currGeojson)
      setGroups(currGroups)
      displaySnackBar('Export OK')
    })
  }

  const gitFunc = () => {
    const config = {headers: { Authorization: `Bearer ${window.localStorage.getItem('minas_token')}` }}
    localFetch.get(id+'/git', config).then(res => {
      console.log(res.data)
      const stderr = (res.data.stderr || '').split("\n")
      const stdout = (res.data.stdout || '').split("\n")
      displaySnackBar([...stderr, ...stdout])
    }).catch(err => {
      displaySnackBar(err, true)
    })
  }

  
  if (!geojson || !groups)
    return <div> is loading... </div>

  return (
    <>
    <Switch>
      <Route exact path={`${match.path}`}>
        <ViewMap key={id} id={id} geojson={geojson} groups={groups}/>
      </Route>
      <Route path={`${match.path}/edit`}>
        {isAuth?
          <EditableMap key={id} id={id} geojson={geojson} groups={groups} saveGroups={setGroups} exportFunc={exportFunc} gitFunc={gitFunc}/> 
        :
          <Redirect to={{pathname: "/"}}/>
        }
      </Route>
    </Switch>
      <div id="snackbar" className={`${snackbarContent?'show':''} ${snackbarError?'error':''} `}>
        {snackbarContent}
      </div>
    </>
  )
}

export default MapContainer