#!/bin/bash

git init
mkdir ~/.ssh 
touch ~/.ssh/id_dsa
echo "Host gitlab.com
  StrictHostKeyChecking no" > ~/.ssh/config
echo "$GIT_PRIVATE_KEY" > ~/.ssh/id_dsa
git remote add origin git@gitlab.com:Annoa/minasmap.git
git fetch
git reset origin/master