/* eslint-disable jsx-a11y/anchor-is-valid */
import { Fragment, useEffect, useState } from 'react'
import axios from 'axios'
import {
  BrowserRouter as Router,
  NavLink,
  Route,
  Switch
} from 'react-router-dom'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

import { getAppUrl } from './helpers/app'


import MapContainer from './Map'
import Snackbar, { useSnackbar } from './Snackbar'


const localFetch = axios.create({ baseURL: getAppUrl() })

function App({
  isAuth,
  AuthButton
}) {
  const [maps, setMaps] = useState(null)
  
  const {displaySnackbar, snackbarContent, snackbarError} = useSnackbar()

  useEffect(() => {
    localFetch.get('maps').then(res => {
      console.log(res.data)
      setMaps(res.data)
    }).catch(err => {
      console.error(err)
      displaySnackbar(['Failed to fetch maps', err?.message], true)
    })
  }, [displaySnackbar])

  if (!maps)
    return <div></div>

  return (
    <Router>
      <div className="App">
        <nav className="App-nav">
          { AuthButton }
          { maps.map(m =>
            <Fragment key={m.id}>
              <NavLink to={`/map/${m.id}`} className="nav-button" 
                activeClassName="nav-button-active"
              >
                {m.name}
              </NavLink>
              {isAuth && 
                <NavLink className="nav-button nav-button-edit" to={`/map/${m.id}/edit`}>
                  <FontAwesomeIcon icon={["fas", "edit"]} />
                </NavLink>
              }
            </Fragment>
          )}
        </nav>
        <Switch>
          <Route path="/map/:id">
            <MapContainer isAuth={isAuth}/>
          </Route>
        </Switch>
        <Snackbar content={snackbarContent} isError={snackbarError}/>
      </div>
    </Router>
  )
}

export default App