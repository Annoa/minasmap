require('dotenv').config()

const express = require('express')
const path = require('path')
const app = express()
const compression = require('compression')
const cors = require('cors')
const server = require('http').createServer(app);
const io = require('socket.io')(server, {cors: {origin: '*'}})
const fs = require('fs')
const jwt = require('./server/login')
const CryptoJS = require("crypto-js")
const sha256 = require('crypto-js/sha256')
const { exec } = require("child_process")


const DATA_PATH = './server/data/'

app.use(cors())
app.options('*', cors())
app.use(express.json())
app.use(express.urlencoded({ extended: false }))
app.use(compression())

app.use(express.static(path.join(__dirname, 'build')))

let emptyGeojson = {
  "type": "FeatureCollection",
  "features": []
}

app.post('/login', (req, res) => {
  console.log('POST /login', req.body)
  const password = req.body.password
  // TODO: si token encore bon
  if (password === sha256(process.env.ADMIN_PASSWORD).toString(CryptoJS.enc.Base64)) {
    const token = jwt.generateAccessToken({ username: 'Admin' });
    res.json(token)
  } else {
    res.sendStatus(401)
  }
})

app.get('/tokenIsValid', jwt.authenticateToken, (req, res) => {
  console.log('GET /tokenIsValid')
  res.sendStatus(200)
})

app.get('/maps', (req, res) => {
	console.log('GET /maps')
  try {
    const data = fs.readFileSync(DATA_PATH+'maps.json', 'utf8')
    res.json(JSON.parse(data))
  } catch (err) {
    console.error(err)
    res.json([])
  }
})

app.get('/:id/geojson', (req, res) => {
	console.log('GET /geojson', req.params.id)
  try { 
    const data = fs.readFileSync(`${DATA_PATH}${req.params.id}/map.geojson`, 'utf8')
    res.json(JSON.parse(data))
  } catch (err) {
    console.error(err)
    res.json(emptyGeojson)
  }
})

app.post('/:id/geojson', jwt.authenticateToken, (req, res) => {
	console.log('POST /geojson', req.body, req.params.id)
  try {
    fs.writeFileSync(`${DATA_PATH}${req.params.id}/map.geojson`, JSON.stringify(req.body))
    res.sendStatus(200)
  } catch (err) {
    console.error(err)
    res.sendStatus(500)
  }
})

app.get('/:id/map.geojson', (req, res) => {
	console.log('GET /:id/map.geojson', req.params.id)
  try {
    res.sendFile(`${DATA_PATH}${req.params.id}/map.geojson`, { root: __dirname })
  } catch (err) {
    console.error(err)
    res.sendStatus(500)
  }
})

app.get('/:id/groups', (req, res) => {
	console.log('GET /groups', req.params.id)
  try {
    const data = fs.readFileSync(`${DATA_PATH}${req.params.id}/groups.json`, 'utf8')
    res.json(JSON.parse(data))
  } catch (err) {
    console.error(err)
    res.json([])
  }
})

app.post('/:id/groups', jwt.authenticateToken, (req, res) => {
	console.log('POST /groups', req.body, req.params.id)
  try {
    fs.writeFileSync(`${DATA_PATH}${req.params.id}/groups.json`, JSON.stringify(req.body))
    res.sendStatus(200)
  } catch (err) {
    console.error(err)
    res.status(500).send(err)
  }
})

app.get('/:id/groups.json', (req, res) => {
	console.log('GET /:id/groups.json', req.params.id)
  try {
    res.sendFile(`${DATA_PATH}${req.params.id}/groups.json`, { root: __dirname })
  } catch (err) {
    console.error(err)
    res.status(500).send(err)
  }
})

app.get('/:id/git', jwt.authenticateToken, (req, res) => {
	console.log('GET /git', req.params.id)
  try {
    exec("sh ./scripts/commitPushMapData.sh "+req.params.id, (error, stdout, stderr) => {
      if (error) {
        throw new Error(error.message)
      }
      // if (stderr) {
      //   console.warn(`stderr: ${stderr}`);
      //   return;
      // }
      console.warn(`stderr: ${stderr}`)
      console.log(`stdout: ${stdout}`)
      res.status(200).json({stderr, stdout})
    })
  } catch (err) {
    res.status(500).send(err)
  }
})

io.on("connection", socket => {
  console.log("New client connected")
  socket.on('ping', data => {
    console.log('ping', data)
  	socket.broadcast.emit('ping', data)
  })
  socket.on("disconnect", () => console.log("Client disconnected"))
})

app.get('*', (req, res) => {
  res.sendFile(path.join(__dirname, 'build', 'index.html'))
})


const PORT = process.env.PORT || 80
server.listen(PORT, () => {
	console.log(`server running on port ${PORT}`)
})