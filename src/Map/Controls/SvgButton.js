/* eslint-disable jsx-a11y/anchor-is-valid */

const SvgButton = ({onClick, children}) => (
  <a href="#" role="button" className="svg-button" onClick={onClick}>
    <div className="svg-icon-container">{children}</div>
  </a>
)

export default SvgButton